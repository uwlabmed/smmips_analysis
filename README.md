# smMIPS pipeline README #

# This is the analytical pipeline used to call variants for these papers:
# "Ultrasensitive detection of acute myeloid leukemia minimal residual disease using single molecule molecular inversion probes"
# version 1.0
# "Ultrasensitive detection of chimerism by single-molecule molecular inversion probe capture and high throughput sequencing of copy number deletion polymorphisms"
# version 2.0
# it now uses fgbio to call consensus reads and picard tools.  The pipeline is much faster but will give slightly different results than the initial version

# create a code directory to clone into (must be named code)
# git clone ssh://git@bitbucket.org/uwlabmed/smmip_analysis.git into that code directory 
# install Dependencies into a common directory:
#    ea-utils.1.1.2-686
#    pear-0.9.4-bin-64.tar
#    bwa-0.7.12
#    samtools-1.1
#    VarScan.v2.3.7
#    pear-0.9.4-bin-64.tar
#    fgbio-0.4.0.jar
#    picard-2.17.2.jar
#    table_annovar.pl Version: $Date: 2015-06-17 21:43:53 -0700 (Wed, 17 Jun 2015)
#    annovar libraries: refGene,segdup,snp138,exac03,dbscsnv11,1000g2015aug_all,1000g2015aug_amr,1000g2015aug_eur,1000g2015aug_eas,
#                       1000g2015aug_sas,1000g2015aug_afr,ljb26_all,esp6500siv2_all,esp6500siv2_aa,esp6500siv2_ea,cosmic70,clinvar_20150629,nci60
#    hg37.fa reference genome
# 
# perl modules Switch and Math::CDF need to be installed
# The read files need to be named in this format ${PFX}_S*_L001_R1_001.fastq.gz where ${PFX} is the <readfile prefix>
# edit source.sh pointing environment variables to the proper places for the above files
#   set SALIPANTE_PROGRAMS_ROOT to where you installed the above dependencies
#   set PERL5LIB to where you have installed the switch and Math::CDF perl modules
#   set ANNOVARLIB_PATH to where you have installed the ANNOVAR libraries 
# create a config.txt in the directory you are going to run the pipeline from has the path to the directory you created the code subdirectory in
# that file simply contains the link to the top of the cloned repo
#
# also note that the pipeline assumes readnames in a format like this "@M00745:142:000000000-D2RPB:1:1101:18248:1845" (format coming from illumina sequencers)
# so if you are using our SRA reads since the rename the reads to look like this "@SRR5220259.1" you need to rename them to look like the above.  Text after the 
# name is not used and doesn't need to be changed. 

# The 2.0 pipeline is used for our copy number paper when you use it with the COPYNUM command line parameter
# Also if you are using this version for the initial paper you will need to use an older version of this pipeline or create a new alpha-beta matrix
# command line:
# nohup <path to script>/smmips_pipeline.sh  <readfile prefix> <directory to write output also where config.txt exists> <path to directory with reads> <path to smmips file in the pipelines/smmips_pipeline/mips_regions.txt> <path to human genome> <COPYNUM or HEME> <version of pipeline to run COUNTONLY, COUNTCONTINUE or DOALL> <# of cores to use>
# example:
#  nohup /home/local/code/pipelines/smmips_pipeline/smmips_pipeline.sh  raji-1 /home/local/smmips /home/local/sequencing_runs/ /home/local/code/pipelines/smmips_pipeline/mips_regions.txt /home/local/databases/hg37/hg37.fa COPYNUM DOALL 24

# for questions about this pipeline send email to waalkes@uw.edu
# for citing this code use the same reference as the papers above
